from pymongo import MongoClient
from materi.books import database as db
import csv

def add_book():
    data = open("bestsellers-with-categories.csv",encoding='utf-8')
    books = csv.reader(data, delimiter=',')
    next(books)

    for book in books:
        try:
            data = {
                "nama": book[0],
                "pengarang": book[1],
                "tahun_terbit": book[5],
                "genre": book[6]
            }
            db.insertBook(data)
        except Exception as e:
            print("Kesalahan pada saat memasukan data: {}".format(e))
            break

def search_books(params):
    for book in db.searchBookByName(params):
        print(book)

def show_book():
    for book in db.showBooks():
        print(book)

def show_id():
    for book in db.showBookById():
        print(book)

def hapus():
    query = {
        'tahun terbit' : '2013'
    }
    print(db.deleteBookById(query))

def new(doc):
    cek = db.updateBookById(doc)
    print(cek)
    

if __name__ == "__main__":
    db = db()
    add_book()
    db.nosql_db.close()