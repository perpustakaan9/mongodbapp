from pymongo import MongoClient

class database:
    def __init__(self):
        try:
            self.nosql_db = MongoClient()
            self.db = self.nosql_db.perpustakaan
            self.mongo_col = self.db.books
            print("database connected")
        except Exception as e:
            print(e)
    
    def showBooks(self):
        result = self.mongo_col.find()
        return result
    
    def showBookById(self, id):
        query = {'_id' : {'$regex' : id}}
        result = self.mongo_col.find(query)
        return result
    
    def searchBookByName(self, key):
        query = {'nama' : {'$regex' : key}}
        result = self.mongo_col.find(query)
        return result
    
    def insertBook(self,document):
        self.mongo_col.insert_one(document)
    
    def updateBookById(self, doc):
        self.mongo_col.update({'pengarang' : "Stephenie Meyer"}, {'pengarang' : doc})
    
    def deleteBookById(self):
        self.mongo_col.remove()